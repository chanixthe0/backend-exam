-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: api_blog
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commentable_id` int(11) NOT NULL,
  `creator_id` int(11) NOT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'awdw awdadwad','App\\Post',1,9,NULL,'2019-12-24 03:43:35','2019-12-24 03:43:35'),(2,'ssssssss','App\\Post',1,9,NULL,'2019-12-24 04:17:23','2019-12-24 04:17:23'),(3,'sdzdzdzsd','App\\Post',1,9,NULL,'2019-12-24 04:17:27','2019-12-24 04:17:27'),(4,'ssss','App\\Post',2,9,NULL,'2019-12-24 04:21:45','2019-12-24 04:21:45'),(5,'awdad','App\\Post',1,9,NULL,'2019-12-24 04:47:15','2019-12-24 04:47:15'),(6,'wadwad','App\\Post',8,9,NULL,'2019-12-24 04:47:29','2019-12-24 04:47:29');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2019_08_19_000000_create_failed_jobs_table',1),(4,'2016_06_01_000001_create_oauth_auth_codes_table',2),(5,'2016_06_01_000002_create_oauth_access_tokens_table',2),(6,'2016_06_01_000003_create_oauth_refresh_tokens_table',2),(7,'2016_06_01_000004_create_oauth_clients_table',2),(8,'2016_06_01_000005_create_oauth_personal_access_clients_table',2),(9,'2019_12_23_144116_add_password_confirmation_to_users_table',3),(10,'2019_12_23_161342_create_posts_table',4),(11,'2019_12_23_162239_add_image_to_posts',5),(12,'2019_12_24_055347_create_comments_table',6),(13,'2019_12_24_111131_add_null_value_to_parent_id',7);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_access_tokens`
--

DROP TABLE IF EXISTS `oauth_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_access_tokens`
--

LOCK TABLES `oauth_access_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_access_tokens` DISABLE KEYS */;
INSERT INTO `oauth_access_tokens` VALUES ('04f6ab6a2a81765e1a474cb6ee85f2fee75fe09ee904edc87a89a192908d2246bb3ee63b1be8373e',9,1,'MyApp','[]',0,'2019-12-24 01:55:14','2019-12-24 01:55:14','2020-12-24 09:55:14'),('07cc5655fc791d995e98541789a21bc6c61a95e23deb320c12e0e327369e0ebbb81f2097c9634270',1,1,'MyApp','[]',0,'2019-12-23 06:45:24','2019-12-23 06:45:24','2020-12-23 14:45:24'),('0d675b59acfab352d6429973318875f54d7794e1282c00fb4eb99e7045c6f5930b1b3ca8a726db03',9,1,'MyApp','[]',0,'2019-12-23 08:00:46','2019-12-23 08:00:46','2020-12-23 16:00:46'),('0f9387fa8c1ef02ca6af0a7bb151190f6d7cc72adfb5e284b51021e40ae21c5755a08112c11f4ce5',9,1,'MyApp','[]',0,'2019-12-23 07:20:27','2019-12-23 07:20:27','2020-12-23 15:20:27'),('14f9fad59c8f0d57f9364a646eb0fd69dbfacbebe5b80ac0bcc47138a5f57b369651fba318bd8f22',9,1,'MyApp','[]',0,'2019-12-23 19:14:14','2019-12-23 19:14:14','2020-12-24 03:14:14'),('18702c69f6a67f8d676baa6876e9dadb3f742bea0fd0e0f2d769daa5439394cd4991cfae5ce0f1d2',9,1,'MyApp','[]',0,'2019-12-23 07:20:41','2019-12-23 07:20:41','2020-12-23 15:20:41'),('1dc7ad052bd7d8ca5de5302ea932cdb0ab8b2e4fa68c649306cbf77ab92b5458ad13110fba8537db',9,1,'MyApp','[]',0,'2019-12-23 08:07:53','2019-12-23 08:07:53','2020-12-23 16:07:53'),('1e490527b4b6c3530f52d99b3a0e7318d1cde25d926e838a701565f0bab9225d65c45e650be802c3',9,1,'MyApp','[]',0,'2019-12-23 18:23:31','2019-12-23 18:23:31','2020-12-24 02:23:31'),('20ade45647cbb8d59f3ea451f9c318fb724186681b17c02fe7517b62e2eee77d6c1d21bbd42970d9',9,1,'MyApp','[]',0,'2019-12-23 21:40:38','2019-12-23 21:40:38','2020-12-24 05:40:38'),('20b5d4273dc92a9953813621f0f506737d957b098d452c3f9986f9e2415c3835d81ac1e1ea665734',9,1,'MyApp','[]',0,'2019-12-23 21:20:32','2019-12-23 21:20:32','2020-12-24 05:20:32'),('25a9fd6245cf64384fe38690956cb114e94c1d958d0b3d089f2d2db08b35187f4025aae6cf0f13e3',9,1,'MyApp','[]',0,'2019-12-23 14:24:44','2019-12-23 14:24:44','2020-12-23 22:24:44'),('28e84a5a0e6a2d045c9b79d84c3fd703b8836ca35d861a2fd0dc41e484210990a1e0514672a716b6',9,1,'MyApp','[]',0,'2019-12-23 17:47:05','2019-12-23 17:47:05','2020-12-24 01:47:05'),('348cc31be0aa5dc191ccd298e899b2da5756c626a48d8cf112748738a78b7e82a280571112a2497e',9,1,'MyApp','[]',0,'2019-12-23 08:31:21','2019-12-23 08:31:21','2020-12-23 16:31:21'),('36251d1c6b2ca256723eb71312d535afc38a250e83011c32dc9eb029c4f7d0e90a5c46502e204f5c',9,1,'MyApp','[]',0,'2019-12-23 14:42:03','2019-12-23 14:42:03','2020-12-23 22:42:03'),('3f6c751f9f7f1ad9e236ab451fbee01427012be2605a3e1f6984f1321e8787b83e3c2d0efa1cf1af',9,1,'MyApp','[]',0,'2019-12-23 14:31:52','2019-12-23 14:31:52','2020-12-23 22:31:52'),('413663474ac503bc41e8404dadf0ea3da2c284a57af7e33d44d2697701652d571466389ead61b90d',9,1,'MyApp','[]',0,'2019-12-23 17:34:55','2019-12-23 17:34:55','2020-12-24 01:34:55'),('44b99559bb6ed53e8e0652e5cca8412cb4ee7c3a0075c98081809034f51574f2b9caf89e0165aa57',9,1,'MyApp','[]',0,'2019-12-23 07:50:24','2019-12-23 07:50:24','2020-12-23 15:50:24'),('47fdc2f24b8b5f12339c4ef5d6d038699ce5498a140476725a24aa97a7fbac0ea4b0dc487d4e4d0b',9,1,'MyApp','[]',0,'2019-12-23 08:10:14','2019-12-23 08:10:14','2020-12-23 16:10:14'),('51bdb5a28d9c211987fe07db4d8d804d063e0d4abd813794f22830b70a2e9397a9a5d0612c0ac094',9,1,'MyApp','[]',0,'2019-12-23 08:09:13','2019-12-23 08:09:13','2020-12-23 16:09:13'),('56bf301b552494b49c8ed00dc499d98471e68a12e9f76cd91859b0fb9b5ea59ba1431044bd93b823',9,1,'MyApp','[]',0,'2019-12-23 14:23:54','2019-12-23 14:23:54','2020-12-23 22:23:54'),('5a788c7e59eb6a0c3ac5ee73da6504648b315ca69d069f47a3ad528ce552aebb681809f957a656d7',9,1,'MyApp','[]',0,'2019-12-23 08:36:04','2019-12-23 08:36:04','2020-12-23 16:36:04'),('5d050e835cf33c78ac34ca793a7fa73dc8913ffd6c8eb986c9098093c23e55bfd2842883271634a6',9,1,'MyApp','[]',0,'2019-12-23 14:51:08','2019-12-23 14:51:08','2020-12-23 22:51:08'),('65621fb3e05ff49ec2d2b8a6f3561031057939952ca41f213dd29c24377c2572f02a28c4006adc2e',9,1,'MyApp','[]',0,'2019-12-23 07:59:36','2019-12-23 07:59:36','2020-12-23 15:59:36'),('6bb08fd1a479b2917dcfe053b18d2814506ca980df8e2d96dde161351e7ae7e8b7980c423ac4d1ae',9,1,'MyApp','[]',0,'2019-12-23 14:30:53','2019-12-23 14:30:53','2020-12-23 22:30:53'),('6d187a529ed6f842b3a32be06d5dd9d109877a63d010142c725e0f6651f2952e5579bf0eef54f5c5',9,1,'MyApp','[]',0,'2019-12-23 14:14:16','2019-12-23 14:14:16','2020-12-23 22:14:16'),('71ca357c534804b88272e2847a55d5dfb20a7ac78904d3828150fd47c1b8a3f8f07059852ef6530d',9,1,'MyApp','[]',0,'2019-12-23 17:29:05','2019-12-23 17:29:05','2020-12-24 01:29:05'),('760dc5bbfa9160f462d02015d45c8b6e0406b724e81ad5c5997b18788d4fb479db86b1ecbb2e9a15',9,1,'MyApp','[]',0,'2019-12-23 17:41:26','2019-12-23 17:41:26','2020-12-24 01:41:26'),('786fadfbf27c1775d685b3cc78eeb9b86a9dd4c6106493c922fddf1db5eb99a923b0fb60305aa87c',9,1,'MyApp','[]',0,'2019-12-23 07:21:17','2019-12-23 07:21:17','2020-12-23 15:21:17'),('79b670922125f1b48f9cc518fe4fa5eb076cbf8badccc962a636d684ae2b3128dc9ef5786173bb8f',9,1,'MyApp','[]',0,'2019-12-23 14:42:20','2019-12-23 14:42:20','2020-12-23 22:42:20'),('7bc8fea184ac566b60137013a83706e885ab55e3bf02016c3b1096ac5241d962a2d29d55b84161d8',9,1,'MyApp','[]',0,'2019-12-23 14:13:29','2019-12-23 14:13:29','2020-12-23 22:13:29'),('7ccaedfc030683a8ac07638765b9d197a7dedb1da2c38fada0b65ddc00b98e6abb80d58c8d69af01',9,1,'MyApp','[]',0,'2019-12-23 07:50:45','2019-12-23 07:50:45','2020-12-23 15:50:45'),('81e680962b99fce79a05c63317025fe23989819a4bb0eb3b81d74adc6d981aad0c0bd4326afaff52',9,1,'MyApp','[]',0,'2019-12-23 21:18:39','2019-12-23 21:18:39','2020-12-24 05:18:39'),('82e45a4b3cd7df94aaaa513ffe2ee4dc06a585390cf780a82c120fff7850b53d5b95dde5a2652761',9,1,'MyApp','[]',0,'2019-12-23 14:25:03','2019-12-23 14:25:03','2020-12-23 22:25:03'),('853e8a62fe9bd216df8671410475385fd72a7070b82af2d341e2efd5f546d397f5dc297e914801de',9,1,'MyApp','[]',0,'2019-12-23 17:42:44','2019-12-23 17:42:44','2020-12-24 01:42:44'),('8828719a425bc875da937efa704bc6d0edcc34e9091595a197ab9f41c3bc8285ae6621ba57d0686b',9,1,'MyApp','[]',0,'2019-12-23 17:32:45','2019-12-23 17:32:45','2020-12-24 01:32:45'),('92e7e8df403a04624472efcf65265f916c8a8336c295e8e1cc87fdc8e8bb0d28d883837277e56d8b',4,1,'MyApp','[]',0,'2019-12-23 06:48:24','2019-12-23 06:48:24','2020-12-23 14:48:24'),('a07899c4ad34a72d6b047f6f7b2a518f3bab427684b625973d6c8b447db7013d46a8711a8f892543',9,1,'MyApp','[]',0,'2019-12-23 17:44:58','2019-12-23 17:44:58','2020-12-24 01:44:58'),('ac29099b16d679d6f8624d97da211f071a1f0067d12c46cb65816aba370826fab68250af52476dd4',9,1,'MyApp','[]',0,'2019-12-23 14:25:06','2019-12-23 14:25:06','2020-12-23 22:25:06'),('ac96c2ce972ce8bfc59faf4442d18e50a76410e1f4549db9d738c9b3d162eb0cfc6b4d4393e206d1',9,1,'MyApp','[]',0,'2019-12-23 07:03:15','2019-12-23 07:03:15','2020-12-23 15:03:15'),('b73190b42b18d00be64294e8c2fe91cd3fec0998de273a44bb72a96c8d486b032b02cce50bb3b17f',9,1,'MyApp','[]',0,'2019-12-24 04:21:55','2019-12-24 04:21:55','2020-12-24 12:21:55'),('b9d2bab7efa588fb92e152047b949dce49c6e441a607fe9e97bd4c03b364be0108214f241a4597d6',9,1,'MyApp','[]',0,'2019-12-24 05:14:43','2019-12-24 05:14:43','2020-12-24 13:14:43'),('bb9265d72a5197bc0d75814558b072b788fb86b8f919f99e31d144855ef2f33320277b9250db42a2',9,1,'MyApp','[]',0,'2019-12-23 14:25:05','2019-12-23 14:25:05','2020-12-23 22:25:05'),('bcd17ee7d73fa1d6c3edafea2e36c0bbfd52b677d0e2ea4630ce2d3c4d3b1f1b38dc04da98e9e930',9,1,'MyApp','[]',0,'2019-12-23 07:53:58','2019-12-23 07:53:58','2020-12-23 15:53:58'),('c02d793686758a6f4f34046a452d0eafd92414d861281fc860753227a0b7f199a3757d2220510ba9',9,1,'MyApp','[]',0,'2019-12-24 03:33:14','2019-12-24 03:33:14','2020-12-24 11:33:14'),('c4a0f1aab0d3de8de4b1913f6093f051d04c098e080483ba86a58206aa42833ed7ec9c22502489ee',9,1,'MyApp','[]',0,'2019-12-23 08:30:24','2019-12-23 08:30:24','2020-12-23 16:30:24'),('c5f7f1d439faa948616aae5a5a3954e13dbb59ce6226835cd0370388ef192c7c61e8054fd5ca4753',9,1,'MyApp','[]',0,'2019-12-23 14:12:02','2019-12-23 14:12:02','2020-12-23 22:12:02'),('ca9617f7a13a61b8529be7ee7d4b8f4b615e77ee1282421f346cd1594374e89a209f18be73231f0c',9,1,'MyApp','[]',0,'2019-12-23 14:23:41','2019-12-23 14:23:41','2020-12-23 22:23:41'),('cb8164eced097de8a18cf7771b32b4308e56a6808b230b87592dec4672191b9c7912f29c00b89b04',9,1,'MyApp','[]',0,'2019-12-23 07:20:10','2019-12-23 07:20:10','2020-12-23 15:20:10'),('d3b1dae6ed64592f8abc032357f0013712b6035f958878359f441b6b420704de897d4b8f9a2fb7f3',9,1,'MyApp','[]',0,'2019-12-23 08:40:52','2019-12-23 08:40:52','2020-12-23 16:40:52'),('d414839aedd81cf866628ca95bb3d11aaf3665a1907e0e3846d4217351b6e49bb88dfcea1dab3633',9,1,'MyApp','[]',0,'2019-12-23 14:34:00','2019-12-23 14:34:00','2020-12-23 22:34:00'),('f3a9e5917ca8f501b4bd354fef259fbe2732dd5fd28283350759d5979ab7604909374cc5eda456dd',9,1,'MyApp','[]',0,'2019-12-24 02:59:00','2019-12-24 02:59:00','2020-12-24 10:59:00'),('fc9b4bb8e740100c8302709fb2d348e7d7ea31de604b07561839c1cd00bca3f6ce6c5fba2745e731',9,1,'MyApp','[]',0,'2019-12-23 07:06:47','2019-12-23 07:06:47','2020-12-23 15:06:47');
/*!40000 ALTER TABLE `oauth_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_auth_codes`
--

DROP TABLE IF EXISTS `oauth_auth_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `client_id` int(10) unsigned NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_auth_codes`
--

LOCK TABLES `oauth_auth_codes` WRITE;
/*!40000 ALTER TABLE `oauth_auth_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_auth_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_clients`
--

DROP TABLE IF EXISTS `oauth_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_clients`
--

LOCK TABLES `oauth_clients` WRITE;
/*!40000 ALTER TABLE `oauth_clients` DISABLE KEYS */;
INSERT INTO `oauth_clients` VALUES (1,NULL,'Laravel Personal Access Client','W8sZ1LQc9fP253VHZNEtwupPLaZHy1j7HVuAf9tW','http://localhost',1,0,0,'2019-12-23 06:32:31','2019-12-23 06:32:31'),(2,NULL,'Laravel Password Grant Client','bhIqTrTcBGwVPPt3iahBWDzdgeI8x4iqgW97qUlx','http://localhost',0,1,0,'2019-12-23 06:32:31','2019-12-23 06:32:31');
/*!40000 ALTER TABLE `oauth_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_personal_access_clients`
--

DROP TABLE IF EXISTS `oauth_personal_access_clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `client_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_personal_access_clients`
--

LOCK TABLES `oauth_personal_access_clients` WRITE;
/*!40000 ALTER TABLE `oauth_personal_access_clients` DISABLE KEYS */;
INSERT INTO `oauth_personal_access_clients` VALUES (1,1,'2019-12-23 06:32:31','2019-12-23 06:32:31');
/*!40000 ALTER TABLE `oauth_personal_access_clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth_refresh_tokens`
--

DROP TABLE IF EXISTS `oauth_refresh_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth_refresh_tokens`
--

LOCK TABLES `oauth_refresh_tokens` WRITE;
/*!40000 ALTER TABLE `oauth_refresh_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth_refresh_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'ssssssssss1','awdad','da',9,'2019-12-23 17:51:35','2019-12-23 17:51:35','wad'),(2,'awdwad','wadwadawd','awdwad',9,'2019-12-23 17:52:08','2019-12-23 17:52:08','awdwad'),(3,'first post','awdadad','first-post',9,'2019-12-23 18:02:42','2019-12-23 18:02:42','awdadad'),(4,'second post','awdad','second-post',9,'2019-12-23 18:10:05','2019-12-23 18:10:05','awdawda'),(5,'awdad','wadad','awdad',9,'2019-12-23 18:10:46','2019-12-23 18:10:46','awdawd'),(6,'awdawd','wad','awdawd',9,'2019-12-23 18:13:11','2019-12-24 05:03:36','wadad'),(7,'awd','adwadawd','awd',9,'2019-12-23 18:14:35','2019-12-23 18:14:35','dwad'),(8,'ad','aawdawd','ad',9,'2019-12-23 18:15:13','2019-12-23 18:15:13','awdwad'),(9,'dad','adadawd','dad',9,'2019-12-23 18:15:41','2019-12-23 18:15:41','wada'),(10,'dadaw','dawda','dadaw',9,'2019-12-23 18:16:37','2019-12-23 18:16:37','wada'),(11,'dadwad','adadad','dadwad',9,'2019-12-23 18:17:45','2019-12-23 18:17:45','awda'),(12,'ada','dadwad','ada',9,'2019-12-23 18:21:35','2019-12-23 18:21:35','wad'),(13,'dawdwa','dawdwadawdawdad','dawdwa',9,'2019-12-23 18:21:56','2019-12-23 18:21:56','awda'),(14,'awdad','adadaw','awdad',9,'2019-12-23 18:22:37','2019-12-23 18:22:37','wad'),(15,'dad','adadwa','dad',9,'2019-12-23 18:23:03','2019-12-23 18:23:03','wada'),(16,'third-post','awdwadwadawdawd','third-post',9,'2019-12-23 18:23:43','2019-12-24 05:03:14','wadad'),(17,'fourth post','awwdadawd','fourth-post',9,'2019-12-23 18:24:05','2019-12-23 18:24:05','awdadwad'),(18,'awdawd','wadaw','awdawd',9,'2019-12-23 18:25:14','2019-12-24 05:03:36','awdwad'),(19,'2qeqe2q','q2eevre','2qeqe2q',9,'2019-12-23 18:27:14','2019-12-23 18:27:14','qe2qe'),(20,'Fifth post','Fifth content','Fifth-post',9,'2019-12-23 21:21:04','2019-12-23 21:21:04','awdawdaw'),(21,'last','awdadawdada lasttt','last',9,'2019-12-24 05:15:44','2019-12-24 05:15:44','Last');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `password_confirmation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Bertrand Kintanar','bertrand_kintanar@ligph.com',NULL,'$2y$10$9gIVzOmge3/9ucofurxsbeJcJxkcThE.keE55h9fXdX9Mjp1eIYn.',NULL,'2019-12-23 06:45:24','2019-12-23 06:45:24','password'),(4,'Bertrand Kintanar','asas@ligph.com',NULL,'$2y$10$RF7PmfwRTXM6URnsObj90O1SZqMY9rAiJK1Z9Ebcj/93zBcewT5xe',NULL,'2019-12-23 06:48:23','2019-12-23 06:48:23','password'),(5,'dwa','wada@adwa.com',NULL,'123',NULL,'2019-12-23 06:53:59','2019-12-23 06:53:59','123'),(6,'wdaawdad','wadwa@adawd.com',NULL,'123',NULL,'2019-12-23 06:54:49','2019-12-23 06:54:49','123'),(7,'sample','sample@sample.com',NULL,'123',NULL,'2019-12-23 06:55:12','2019-12-23 06:55:12','123'),(8,'Bertrand Kintanar','sdawadawd@ligph.com',NULL,'password',NULL,'2019-12-23 06:56:38','2019-12-23 06:56:38','password'),(9,'Bertrand Kintanar','ss@ss.com',NULL,'$2y$10$0foFuRbBDQQCrdQMPoiCB.UHHL/27wi9V/FboEL9LGYuHLaNVfHuK',NULL,'2019-12-23 07:02:53','2019-12-23 07:02:53','password'),(10,'Bertrand Kintanar','ass@ss.com',NULL,'$2y$10$XfqWS1TA7IlehLaaVhJJ9eHXccSUGRKOr32Buuj5f0vcwvpl0Xl2O',NULL,'2019-12-23 14:39:36','2019-12-23 14:39:36','password');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-24 21:50:56
