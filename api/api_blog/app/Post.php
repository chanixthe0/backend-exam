<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Post extends Model
{
    use HasApiTokens, Notifiable;
    use Notifiable;

    protected $fillable = [
        'title', 'content', 'image', 'slug', 'user_id',
    ];

    public function comments() {
        return $this->morphMany(Comment::class, 'commentable');
    }

    
}
