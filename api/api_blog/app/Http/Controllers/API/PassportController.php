<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\User;

use Illuminate\Support\Facades\Auth;

use Validator;

class PassportController extends Controller

{



    public $status;

   //LOGIN
    public function login( Request $request){

        $res = array();


        if( !$request->validate([ 'password'=> 'required']) && !$request->validate([ 'email'=> 'required']) ){

            $res["message"]                 = "The given data was invalid.";
            $res["errors"]["email"]         = "The email field is required.";
            $res["errors"]["password"]      = ["The password field is required."];
            $this->status                   = 422;

        } else if( !$request->validate([ 'email'=> 'required']) ){

            $res["message"]                 = "The given data was invalid.";
            $res["errors"]["email"]         =  ["The email field is required."];
            $this->status                   = 422;

        } else if( !$request->validate([ 'password'=> 'required']) ){

            $res["message"]                 = "The given data was invalid.";
            $res["errors"]["password"]      = ["The password field is required."];
            $this->status                   = 422;

        } else if( !$request->validate([ 'password'=> 'required']) && !$request->validate([ 'email'=> 'required']) ){

            $res["message"]                 = "The given data was invalid.";
            $res["errors"]["email"]         = "The email field is required.";
            $res["errors"]["password"]      = ["The password field is required."];
            $this->status                   = 422;

        } else if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){

            $user                           = Auth::user();
            $objToken                       = $user->createToken('MyApp');
            $strToken                       = $objToken->accessToken;

            $res['token']                   =  $strToken;
            $res['token_type']              = "bearer";
            $res['expires_at']              = $objToken->token->expires_at;
            $this->status                   = 200; 

        } else{

            $res["message"]                 = "The given data was invalid.";
            $res["errors"]["email"]         = ["These credentials do not match our records."];
            $this->status                   = 422;
        }

        return response()->json($res, $this->status);
        

   }

   //REGISTER
   public function register(Request $request)

   {

        $res = array();
        
        if( $request->name == "" || $request->name == null ){
            
            $res["message"]             = "The given data was invalid.";
            $res["errors"]["name"]      = ["The name field is required."];
            $this->status               = 422; 
            
        }
        else if( User::where( 'email', '=', $request->email )->count() > 0 ){

            $res["message"]             = "The given data was invalid.";
            $res["errors"]["email"]     = ["The email has already been taken."];
            $this->status             = 422; 

        } else if( $request->password != $request->password_confirmation ){

            $res["message"]             = "The given data was invalid.";
            $res["errors"]["password"]  = ["The password confirmation does not match."];
            $this->status             = 422; 

        } else {

            $input = $request->all();
            $input['password'] = bcrypt($input['password']);
        
            $reg_user                    = User::create( $input );
            $res                         = User::select('name', 'email', 'updated_at', 'created_at', 'id')->where("id", $reg_user->id)->get()->toArray();
            $res                         = $res[0];
            $this->status                = 201; 
            
        }
        
        return response()->json( $res, $this->status );

   }

   //GET DETAILS
    public function getDetails()

    {

        $user = Auth::user();

        return response()->json(['success' => $user], $this->successStatus);

    }

    //LOGOUT
    public function logoutApi()
    { 
        if (Auth::check()) {

            Auth::user()->AauthAcessToken()->delete();

        }
    }

}