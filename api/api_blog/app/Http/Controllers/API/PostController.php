<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Validator;

class PostController extends Controller

{

    public $status;
    public $res = array();


    //DISPLAY POSTS
    public function index(){

        $posts_list = Post::paginate(15);
        return response()->json( $posts_list, 200 );
    }   
   
    //VIEW POST
    public function view_post( $slug ){

        $this->res["data"]    = Post::where( "slug", $slug )->first();
        $this->status         = 200;
        
        return response()->json( $this->res, $this->status );

    }

    //CREATE POST
    public function posts(Request $request)
    {

        if( $request->title == "" && $request->content == ""){

            $this->res["message"]               = "The given data was invalid.";
            $this->res["errors"]["title"][]     = "The title field is required.";
            $this->res["errors"]["content"][]   = "The content field is required.";
            $this->status                       = 422; 

        } else if( $request->title == "" ){
            
            $this->res["message"]               = "The given data was invalid.";
            $this->res["errors"]["content"][]   = "The content field is required.";
            $this->status                       = 422; 

        } else {

            $input                  = $request->all();
            $input["slug"]          = str_replace(" ", "-", $input["title"]); 
            $input["user_id"]       = Auth::id();
            $this->res["data"]      = Post::create($input);
            $this->status           = 201; 

        }
        
        return response()->json( $this->res, $this->status );

    }

    //UPDATE POST
    public function patch_post($slug, Request $request)
    {   
        $upt_post = Post::where('slug', $slug)->update($request->toArray());

        if($upt_post) {
            $this->res["data"]           = Post::where('slug', $request->get('slug'))->first();
            $this->status                = 200;
        } else {
            $this->res["message"]        = "No post found";
            $this->status                = 422;
        }

        return response()->json( $this->res, $this->status );
    }


    //DELETE POST
    public function delete_post($slug)
    {   
        $delete_post = Post::where( "slug", $slug )->delete();

        if( $delete_post ){
            $this->res["status"]         = "Record deleted successfully";
            $this->status                = 200;

        } else {
            $this->res["status"]         = "Failed to delete";
            $this->status                = 422;
        }

        return response()->json( $this->res, $this->status  );
    }

}