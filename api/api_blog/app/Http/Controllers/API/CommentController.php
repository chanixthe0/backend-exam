<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Validator;

class CommentController extends Controller
{

    public $res;
    public $status_code;


    public function index($slug)
    {
    
        $post = Post::where('slug', $slug)->first();

        if( $post ){
            $this->res["data"]      = $post->comments()->get();
            $this->status_code      = 200;
        
        } else {
            $this->res['message']   = "No query results for model [".Post::class."]";
            $this->status_code      = 404;
        }

        return response()->json( $this->res, $this->status_code );
        
    }

    //CREATE COMMENT
    public function create_comment( Request $request, $slug ){
        
        if( $request->body == "" ){

            $this->res["message"] = "The given data was invalid.";
            $this->res["errors"]["body"][] = "The body field is required.";
             
        } else {

            $post = Post::where( "slug", $slug )->first();
            $this->res["data"] = $post->comments()->create([
                'commentable_id' => $post->id,
                'body'=> $request->body,
                'creator_id'=> $request->user()->id,
                'parent_id'=> null
            ]);
    
        }

        return response()->json( $this->res, 200 );

    }


    //UPDATE COMMENT
    public function update_comment( $slug, $id, Request $request ){
        
        $post = Post::where('slug',$slug)->first();

        if($post) {

        	$comment = $post->comments()->find($id);
            
            if($comment) {
	        	$response = $comment->update($request->toArray());
                $this->res["data"] = $post->comments()->find($id);
                $this->status_code = 200;
            }
            
            $this->res["message"] = "No query result for model [".Comment::class."]";
            $this->status_code = 404;

        } else {

            $this->res["message"] = "No query result for model [".Post::class."]";
            $this->status_code = 404;

        }

        return response()->json( $this->res, $this->status_code );

    }



    //DELETE COMMENT
    public function delete_comment( $slug, $id ){

        $post = Post::where('slug',$slug)->first();

        if($post) {

            $comment = $post->comments()->find($id);
            
        	if($comment) {
        		$deleted = $comment->delete();
                $this->res["status"]    = 'Record deleted successfully';
                $this->status_code      = 200;
            }
            
        } else {
            $this->res["status"]        = 'No record deleted';
            $this->status_code          = 422;
        }

        return response()->json( $this->res, $this->status_code );
    
    }
   
}