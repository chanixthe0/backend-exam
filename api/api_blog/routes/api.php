<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', 'API\PassportController@login');
Route::post('register', 'API\PassportController@register');
Route::get('logout','API\PassportController@logoutApi');

Route::get('posts', 'API\PostController@index');
Route::get('posts/{slug}/comments', 'API\CommentController@index');
Route::get('posts/{slug}', 'API\PostController@view_post');

Route::group(['middleware' => 'auth:api'], function(){
    
    Route::post('posts', 'API\PostController@posts');
    Route::patch('posts/{slug}', 'API\PostController@patch_post');
    Route::delete('posts/{slug}', 'API\PostController@delete_post');
    Route::post('posts/{slug}/comments', 'API\CommentController@create_comment');
    Route::post('posts/{slug}/comments/{id}', 'API\CommentController@delete_comment');
    Route::patch('posts/{slug}/comments/{id}','API\CommentController@update_comment');
    
});


